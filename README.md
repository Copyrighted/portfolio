# If you're viewing this I've migrated to Github. https://github.com/Copyrighted/portfolio

# Flask Blog Website

Personal flask blog that supports registering/login of an admin user (can easily expand this to non-admin users).

Users can create, update and delete posts.

## Getting Started

### Prerequisites

Python 3.x

Flask

### Installing

Install Python 3.x

Select your favorite IDE (I use PyCharm so that is what I'll be doing the instructions in)

git clone https://gitlab.com/Copyrighted/portfolio

Open project in IDE

Set up Python Virtual Environment, install Flask and other prerequisites

## Running the tests

-Work in progress-

### Break down into end to end tests

-Work in progress-

### And coding style tests

-Work in progress-

## Deployment

-Work in Progress, probably going to use Docker-

## Built With

* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - Backend framework used
* [SQLAlchemy](https://www.sqlalchemy.org/) - ORM Used
* [SQLite](https://sqlite.org/index.html) - Database used
* [Summernote](https://summernote.org/) - Markdown Editor used
* [Jinja2](https://jinja.palletsprojects.com/en/2.11.x/) - Jinja2 used for templating
* [Bcrypt](http://bcrypt.sourceforge.net/) - Bcrypt used to salt/hash passwords

## Authors

* **Tyler Scheffler** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Thanks to hackersandslackers.com for in depth info on Flasks inner workings. [HackersandSlackers](https://hackersandslackers.com/managing-user-session-variables-with-flask-sessions-and-redis/)

